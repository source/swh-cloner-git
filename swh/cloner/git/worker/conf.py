# Copyright (C) 2015  The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information


import os

from swh.core import config


# Default configuration file
DEFAULT_CONF_FILE = '~/.config/swh/clones-worker.ini'


# default configuration (can be overriden by the DEFAULT_CONF_FILE)
DEFAULT_CONF = {
    # mount folder where to dump the clones
    'mount': ('string', '/tmp/swh-git-cloner/'),
    # witness file built after the cloning. It's a witness of a success clone
    'witness-file-name': ('string', 'witness'),
    # the ssh access key to the ssh-host
    'ssh-access-command': ('string',
                           'ssh -i ~/.ssh/inria/id-rsa-swh-rsync -l swh'),
    # ssh host
    'ssh-host': ('string', 'tytso.inria.fr'),
    # destination folder on host
    'ssh-host-destination-folder': ('string', ''),
    # url access to db
    'db_url':
    ('string',
     'host=<host> port=<port> dbname=<db> user=<usr> password=<pass>'),
    # max file size limit allowed to git clone, in bytes
    'clone_limit_size': ('int', '4294967296'),  # 4 GB
    # the queue url to access for consuming tasks
    'queue_url': ('string', 'amqp://guest:guest@localhost:5672//'),
    # soft time limit for a task, if exceeded, the worker cleans up
    # and stops
    'task_soft_time_limit': ('int', '3600'),
}


def read_conf(conf_file=DEFAULT_CONF_FILE):
    """Read the user's configuration file.

    args contains the repo to parse.
    Transmit to the result.
    """
    return config.read(os.path.expanduser(conf_file), DEFAULT_CONF)
