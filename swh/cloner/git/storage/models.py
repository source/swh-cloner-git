# Copyright (C) 2015  The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from swh.cloner.git.storage import db


def load_repos(db_conn):
    """List the repository.
    Limit the number of repository to load.
    """
    yield from db.query_fetch(
        db_conn,
        'select id, full_name from missing_orig_repos order by id')


def persist_task_result(db_conn, repo_id, task_id, task_start_date,
                        task_duration, status, json_result, stdout, stderr):
    """Persist the task's result.
    """
    return db.query_execute(db_conn, ("""INSERT INTO crawl_history
                                        (repo, task_id, date, duration, status,
                                         result, stdout, stderr)
                                        VALUES(%s, %s, %s, %s, %s,
                                               %s, %s, %s)
                                     """,
                                      (repo_id,
                                       task_id,
                                       task_start_date,
                                       task_duration,
                                       status,
                                       json_result,
                                       stdout,
                                       stderr)))
