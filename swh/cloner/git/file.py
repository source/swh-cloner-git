# Copyright (C) 2015  The Software Heritage developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
import shutil
import os

from . import proc


def touch(path):
    """Create an empty file.
    """
    logging.info('Create temporary file %s' % path)
    open(path, 'a').close()


def rsync(host, access_command, from_folder, to_folder):
    """Execute a rsync action from a source from to a to_folder over the host.
    """
    return proc.execute(['rsync',
                         '-az',
                         '--delete',
                         '-e',
                         access_command,
                         '--relative',
                         from_folder,
                         '%s:%s' % (host, to_folder)])


def rm(folder):
    """Remove a folder from the local filesystem.
    """
    if os.path.exists(folder):
        logging.info('task cleanup %s' % folder)
        shutil.rmtree(folder)
    else:
        logging.info('Folder %s does not exist. Nothing to clean up.' % folder)


def count_size_in_bytes(folder):
    """Compute the bytes size of a folder.
    """
    return int(proc.execute(['du', '-bs', folder]).split()[0])
