#!/usr/bin/env bash

# script to run as root

yum groupinstall -y "Development tools"
yum install -y https://dl.iuscommunity.org/pub/ius/stable/CentOS/6/x86_64/ius-release-1.0-14.ius.centos6.noarch.rpm
yum install -y emacs python34u python34u-devel python34u.pip postgresql-devel libffi-devel git cmake libssh2-devel htop
pip3 install --upgrade pip
pip3 install ipython celery psycopg2 cffi psutil requests

# libgit2 version in yum is 0.20 (incompatible with pygit2 from pip3 0.20)
# source: http://www.pygit2.org/install.html#version-numbers
# So compile libgit2 0.22

mkdir -p build
wget https://github.com/libgit2/libgit2/archive/v0.22.0.tar.gz
tar xzf v0.22.0.tar.gz -C build
cd build/libgit2-0.22.0/
export LIBGIT2=/usr/local
cmake . -DCMAKE_INSTALL_PREFIX=$LIBGIT2 -DLIB_INSTALL_DIR=$LIBGIT2/lib64
make
make install

export LDFLAGS="-Wl,-rpath='$LIBGIT2/lib64',--enable-new-dtags $LDFLAGS"
pip3 install pygit2
